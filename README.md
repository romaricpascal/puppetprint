Puppet Print
===

A little server to print web pages with [Puppeteer].

[Puppeteer]: https://github.com/puppeteer/puppeteer/

Usage
---

TBD

Contributing
---

See [CONTRIBUTING.md](CONTRIBUTING.md)
