import sinon from 'sinon';

describe('test setup', function () {
  describe('mocha', function () {
    it('is set up', function () {
      expect(true).to.be.true;
    });
  });
  describe('sinon', function () {
    it('is set up too', function () {
      const spy = sinon.spy();
      spy();
      expect(spy).to.have.been.called;
    });
  });
});
