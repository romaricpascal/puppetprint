#!/usr/bin/env node

import express from 'express';
import puppeteer from 'puppeteer';
import mergeOptions from 'merge-options';
import awaitjs from '@awaitjs/express';
const { wrap } = awaitjs;

const app = express();

const port = process.env.PORT || 8080;

const DEFAULT_PDF_OPTIONS = {
  format: 'a4',
  margin: {
    top: '2cm',
    left: '2cm',
    right: '2cm',
    bottom: '2cm',
  },
  printBackground: true,
};

// https://github.com/puppeteer/puppeteer/blob/v10.0.0/docs/api.md#pagepdfoptions
const BOOLEAN_OPTIONS = [
  'displayHeaderFooter',
  'printBackground',
  'landscape',
  'omitBackground',
];

const OBJECT_OPTIONS = ['margin'];

const NUMBER_OPTIONS = ['scale'];

function parseOptions(optionsWithStringValues, optionsList, parse) {
  for (const optionName of optionsList) {
    if (
      optionName in optionsWithStringValues &&
      typeof optionsWithStringValues[optionName] == 'string'
    ) {
      optionsWithStringValues[optionName] = parse(
        optionsWithStringValues[optionName],
      );
    }
  }
}

function parsePDFOptions(pdfOptionsWithStringValues) {
  parseOptions(
    pdfOptionsWithStringValues,
    BOOLEAN_OPTIONS,
    (value) => value != 'false',
  );
  parseOptions(pdfOptionsWithStringValues, NUMBER_OPTIONS, parseFloat);
  parseOptions(pdfOptionsWithStringValues, OBJECT_OPTIONS, JSON.parse);
  return pdfOptionsWithStringValues;
}

async function makePDF(url, pdfOptions) {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  });

  try {
    const page = await browser.newPage();
    const response = await page.goto(url, {
      waitUntil: 'networkidle2',
    });

    // Little failsafe to avoid printing error pages
    // TODO: Provide more granular configuration and ways to disable
    if (response.status() >= 400) {
      throw new Error('Page is an error page');
    }

    const buffer = await page.pdf(pdfOptions);

    return buffer;
  } finally {
    // Ensure browsers get closed appropriately
    await browser.close();
  }
}

app.use(
  wrap(async (req, res) => {
    const { url, ...pdfOptionsQueryParams } = req.query;
    const pdfOptions = mergeOptions(
      {},
      DEFAULT_PDF_OPTIONS,
      parsePDFOptions(pdfOptionsQueryParams),
    );

    if (url) {
      const pdf = await makePDF(url, pdfOptions);
      res.setHeader('Content-Type', 'application/pdf');
      res.send(pdf);
    } else {
      res.status(422);
      res.send('Please provide a URL parameter');
    }
  }),
);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
