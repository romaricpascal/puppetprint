module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:node/recommended',
    'plugin:import/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  rules: {
    // Because the .prettierrc file is `.cjs`
    'prettier/prettier': ['error', require('./.prettierrc.cjs')],
    // Force extensions in imports to ensure library work OK when `import`ed in browser
    'import/extensions': ['error', 'ignorePackages'],
    'no-shadow': ['error', { builtinGlobals: true, hoist: 'all', allow: [] }],
  },
  overrides: [
    {
      files: ['*.test.js'],
      extends: ['plugin:mocha/recommended'],
      parserOptions: {
        // Re-set ecmaVersion that's being unset somehow
        ecmaVersion: 2020,
      },
      env: {
        browser: false,
      },
      globals: {
        expect: 'readonly',
      },
      rules: {
        'node/no-unpublished-import': [
          'error',
          {
            allowModules: ['sinon'],
          },
        ],
      },
    },
  ],
};
